package cart

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCartEmpty(t *testing.T) {

	c := ShopeeCart{}
	assert.Equal(t, true, c.IsEmpty())
	assert.Equal(t, 0, c.GetTotalQuantity())
	assert.Equal(t, 0.0, c.GetTotalPrice())
}

func TestCartWithSingleProduct(t *testing.T) {

	c := ShopeeCart{}
	apple := ProductWarehouse{
		Name:  "apple",
		Price: 10,
	}
	assert.Nil(t, c.AddItem(apple, 1))
	assert.Equal(t, false, c.IsEmpty())
	assert.Equal(t, 1, c.GetTotalQuantity())
	assert.Equal(t, 10.0, c.GetTotalPrice())
}

func TestCartWithSingleProductAndQuantity(t *testing.T) {

	c := ShopeeCart{}
	apple := ProductWarehouse{
		Price: 10,
	}
	assert.Nil(t, c.AddItem(apple, 5))
	assert.Equal(t, false, c.IsEmpty())
	assert.Equal(t, 5, c.GetTotalQuantity())
	assert.Equal(t, 50.0, c.GetTotalPrice())
}

func TestCartWithMultiProductAndQuantity(t *testing.T) {

	c := ShopeeCart{}
	apple := ProductWarehouse{
		Name:  "apple",
		Price: 10,
	}
	banana := ProductWarehouse{
		Name:  "banana",
		Price: 20,
	}
	assert.Nil(t, c.AddItem(apple, 5))
	assert.Nil(t, c.AddItem(banana, 3))
	assert.Equal(t, false, c.IsEmpty())
	assert.Equal(t, 8, c.GetTotalQuantity())
	assert.Equal(t, 110.0, c.GetTotalPrice())
}

func TestCartWithAddSameProductMultipleTime(t *testing.T) {

	c := ShopeeCart{}
	apple := ProductWarehouse{
		Name:  "apple",
		Price: 10,
	}
	banana := ProductWarehouse{
		Name:  "banana",
		Price: 20,
	}
	apple2 := ProductWarehouse{
		Name:  "apple",
		Price: 10,
	}
	apple3 := ProductWarehouse{
		Name:  "apple",
		Price: 10,
	}
	assert.Nil(t, c.AddItem(apple, 5))
	assert.Nil(t, c.AddItem(banana, 3))
	assert.Nil(t, c.AddItem(apple2, 10))
	assert.Nil(t, c.AddItem(apple3, 20))
	assert.Equal(t, false, c.IsEmpty())
	assert.Equal(t, 38, c.GetTotalQuantity())
	assert.Equal(t, 410.0, c.GetTotalPrice())
}

func TestCartWithRemoveSomeProductByName(t *testing.T) {

	c := ShopeeCart{}
	apple := ProductWarehouse{
		Name:  "apple",
		Price: 10,
	}
	banana := ProductWarehouse{
		Name:  "banana",
		Price: 20,
	}
	assert.Nil(t, c.AddItem(apple, 5))
	assert.Nil(t, c.AddItem(banana, 3))
	assert.Nil(t, c.RemoveItemByName("apple"))
	assert.Equal(t, false, c.IsEmpty())
	assert.Equal(t, 3, c.GetTotalQuantity())
	assert.Equal(t, 60.0, c.GetTotalPrice())
}

func TestCartWithRemoveAllProductByName(t *testing.T) {

	c := ShopeeCart{}
	apple := ProductWarehouse{
		Name:  "apple",
		Price: 10,
	}
	banana := ProductWarehouse{
		Name:  "banana",
		Price: 20,
	}
	assert.Nil(t, c.AddItem(apple, 5))
	assert.Nil(t, c.AddItem(banana, 3))
	assert.Nil(t, c.RemoveItemByName("apple"))
	assert.Nil(t, c.RemoveItemByName("banana"))
	assert.Equal(t, true, c.IsEmpty())
	assert.Equal(t, 0, c.GetTotalQuantity())
	assert.Equal(t, 0.0, c.GetTotalPrice())
}
